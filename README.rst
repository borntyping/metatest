========
metatest
========

.. image:: https://pypip.in/v/metatest/badge.png
    :target: https://pypi.python.org/pypi/metatest
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/metatest.png
   :target: https://travis-ci.org/borntyping/metatest

A test runner for your test runners.

Requirements
============

metatest currently requires ``click`` and ``colorlog``. These requirements are likely to change between now and version 1.

Compatibility
=============

metatest is built and tested on Python 3.4. While compatibility my be extended to earlier versions of Python 3, a Python 2 port is unlikley.

Tests
=====

There are currently no tests. In the future, metatest will be self-tested ;)

Licence
=======

Copyright (c) 2014 Sam Clements <sam@borntyping.co.uk>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Authors
=======

metatest was written by `Sam Clements <https://github.com/borntyping>`_, with design assistance from `Jon Parish <https://github.com/jonparish>`_.

.. image:: https://avatars.githubusercontent.com/u/249351?s=40
.. image:: https://avatars.githubusercontent.com/u/3470718?s=40
