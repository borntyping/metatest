#!/usr/bin/env python3.4

import logging
import json
import queue
import subprocess
import threading

import click
import colorlog

LOG_FORMAT = '%(asctime)s [%(name)-16s] %(log_color)s%(message)s%(reset)s'

logging.addLevelName(5, 'trace')


class Task(object):
    @classmethod
    def from_description(cls, description):
        return cls(*description.split(' @ '))

    def __init__(self, *pipeline):
        self.pipeline = pipeline
        self.log = logging.getLogger('metatest.task')

    @property
    def command(self):
        return self.pipeline[0]

    @property
    def name(self):
        return self.command.split()[0]

    def run(self):
        self.log.debug("Running command: {}".format(self.command))

        process = subprocess.Popen(
            self.command,
            shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        process.wait()

        if process.returncode != 0:
            self.log.warn("Task '{}' failed with exit code {}".format(
                self.command, process.returncode))
        else:
            self.log.debug("Completed command: {}".format(self.command))

        output = [line.decode('utf-8') for line in process.stdout.readlines()]

        for line in output:
            self.log.log(5, 'STDOUT: {}'.format(line.rstrip()))

        if self.pipeline[1:]:
            return [self.task_from_line(line) for line in output]

        return []

    def task_from_line(self, line):
        command = '{} {}'.format(self.pipeline[1], line.strip())
        return Task(command, *self.pipeline[2:])


class TaskRunner(object):
    def __init__(self, tasks):
        self.tasks = queue.Queue()
        self.tasks_done = queue.Queue()

        for task in tasks:
            self.tasks.put(task)

    def __call__(self):
        while not self.tasks.empty():
            task = self.tasks.get()
            for new_task in task.run():
                self.tasks.put(new_task)
            self.tasks_done.put(task)
            self.tasks.task_done()

    def join(self):
        return self.tasks.join()


@click.command()
@click.option(
    '-c', '--config', default='.metatest.json', type=click.File(),
    help='Path(s) to config file')
@click.option(
    '-w', '--workers', default=1, type=click.INT,
    help='The number of tasks to run at the same time')
@click.option(
    '-l', '--log-level', default='info',
    type=click.Choice(['debug', 'info', 'warn', 'critical', 'error']),
    help='Sets the minimum message level for log messages to be output')
def metatest(config, workers, log_level):
    colorlog.basicConfig(level=log_level.upper(), format=LOG_FORMAT)

    tasks = json.load(config)
    runner = TaskRunner([Task.from_description(t) for t in tasks])

    log = logging.getLogger('metatest')
    log.info('Running {} tasks with {} workers'.format(
        runner.tasks.qsize(), workers))

    for i in range(workers):
        threading.Thread(target=runner).start()
    runner.join()

    log.info('Finished running {} tasks'.format(runner.tasks_done.qsize()))


def main():
    metatest(auto_envvar_prefix='METATEST')

if __name__ == '__main__':
    main()
